import { derived, Readable } from 'svelte/store'
import { allData } from './data'

export interface Histogram {
    bins: number[],
    min: number,
    max: number,
    binSize: number
}

export const numberOfBins = 16

const EMPTY: Histogram = { bins: new Array(numberOfBins).fill(0), min: 0, max: 1, binSize: 1 / numberOfBins }

export const histogram: Readable<Histogram> =
    derived(allData, ($data) => {
        if ($data === undefined) {
            return EMPTY
        }

        const sortedValues = $data.matrix.elements.filter((el) => el > 0)
        sortedValues.sort((a, b) => a - b)

        if (sortedValues.length === 0) {
            return EMPTY
        }

        let min = sortedValues[0]
        const max = sortedValues[sortedValues.length - 1]
        if (min === max) {
            min -= 0.1
        }

        const binSize = (max - min) / numberOfBins

        const bins = new Array(numberOfBins).fill(0)
        for (const value of sortedValues) {
            let i = Math.floor((value - min) / binSize)
            if (i >= bins.length) {
                // Grensgevalletje
                i = bins.length - 1
            }
            bins[i] = bins[i] + 1
        }

        return { bins, min, max, binSize }
    })