import { derived, Readable, writable } from 'svelte/store'
import type { XY } from './graph/drag'

export interface BoundingBox {
    x: number
    y: number
    width: number
    height: number
}

/**
 * Creates a custom store for the transformation matrix
 * with functions for panning and zooming the transformation matrix.
 */
export function zoomStore() {
    let clientWidth: number
    let boundingBox: BoundingBox

    let transformFit: DOMMatrix = new DOMMatrix();
    let transformManual: DOMMatrix

    let panOrigin: XY = undefined
    let transformPanning: DOMMatrix

    const transformStore = writable<DOMMatrix>(transformFit)

    function setClientWidth(width: number) {
        clientWidth = width
        updateFit()
    }

    function setBoundingBox(box: BoundingBox) {
        if (!box || !box.width) {
            return
        }

        // Make square
        if (box.width > box.height) {
            const extra = box.width - box.height
            boundingBox = {
                x: box.x,
                y: box.y - extra / 2,
                width: box.width,
                height: box.height + extra
            }
        } else {
            const extra = box.height - box.width
            boundingBox = {
                x: box.x - extra / 2,
                y: box.y,
                width: box.width + extra,
                height: box.height
            }
        }

        updateFit()
    }

    function updateFit() {
        if (!clientWidth || !boundingBox || !boundingBox.width) {
            return
        }

        let { x, y, width } = boundingBox

        const scale = clientWidth / width

        const dw = Math.abs(width - clientWidth / transformFit.a)
        const dx = Math.abs(-x - transformFit.e / transformFit.a)
        const dy = Math.abs(-y - transformFit.f / transformFit.a)

        if (dw < 0.1 && dx < 0.1 && dy < 0.1) {
            // No significant change, break update loop
            return
        }

        transformFit = new DOMMatrix()
            .scaleSelf(scale)
            .translateSelf(-x, -y)

        updateTransform()
    }

    function panStart(e: CustomEvent<XY>) {
        panOrigin = e.detail
    }

    function panToBoundingBox(transform?: DOMMatrix) {
        if (!transform) {
            return
        }

        const topLeft = transform.transformPoint(boundingBox)
        const bottomRight = transform.transformPoint({
            x: boundingBox.x + boundingBox.width,
            y: boundingBox.y + boundingBox.height
        })

        if (topLeft.x > 0) {
            transform.e -= topLeft.x
        } else if (bottomRight.x < clientWidth) {
            transform.e += (clientWidth - bottomRight.x)
        }

        if (topLeft.y > 0) {
            transform.f -= topLeft.y
        } else if (bottomRight.y < clientWidth) {
            transform.f += (clientWidth - bottomRight.y)
        }
    }

    function panMove(e: CustomEvent<XY>) {
        const { x, y } = e.detail
        const tx = x - panOrigin.x
        const ty = y - panOrigin.y

        if (Math.abs(tx) > 1 || Math.abs(ty) > 1) {
            const offset = new DOMMatrix().translateSelf(x - panOrigin.x, y - panOrigin.y)
            if (transformManual === undefined) {
                transformManual = transformFit
            }

            transformPanning = offset.multiply(transformManual)
            updateTransform()
        }
    }

    function panEnd(e: CustomEvent<XY>) {
        const { x, y } = e.detail
        const tx = x - panOrigin.x
        const ty = y - panOrigin.y

        if (Math.abs(tx) > 1 || Math.abs(ty) > 1) {
            const offset = new DOMMatrix().translateSelf(tx, ty)
            if (transformManual === undefined) {
                transformManual = transformFit
            }

            transformManual = offset.multiply(transformManual)
            updateTransform()
        }

        panOrigin = undefined
        transformPanning = undefined
    }

    /**
     * Zoom in (factor > 1) or out (factor < 1) from a certain origin, or from the center if no origin specified.
     */
    function zoom(factor: number, origin?: XY) {
        if (factor < 1 && transformManual === undefined) {
            return
        }

        if (transformManual === undefined) {
            transformManual = DOMMatrix.fromMatrix(transformFit)
        }

        const { x, y } = origin ?? { x: clientWidth / 2, y: clientWidth / 2 }
        const scaleMatrix = new DOMMatrix().scale(factor, factor, 1, x, y, 0)

        const scaleFactor = transformManual.a * scaleMatrix.a / transformFit.a

        if (factor < 1 && scaleFactor < 1) {
            // Don't zoom out more than fit
            zoomFit()
            return
        }

        if (factor > 1 && clientWidth / transformManual.a * scaleMatrix.a < 300) {
            // Don't zoom in less than 300px wide
            return
        }

        transformManual = transformManual.preMultiplySelf(scaleMatrix)

        updateTransform()
    }

    function zoomFit() {
        transformManual = undefined
        updateTransform()
    }

    /**
     * Return the transformation matrix that should currently be applied (either panning, or zoomed in/out, or fit all).
     */
    function updateTransform() {
        panToBoundingBox(transformPanning)
        panToBoundingBox(transformManual)
        transformStore.set(transformPanning ?? transformManual ?? transformFit)
    }

    return {
        setClientWidth,
        setBoundingBox,
        panStart,
        panMove,
        panEnd,
        zoom,
        zoomFit,
        subscribe: transformStore.subscribe
    }
}

export function axis(transform: Readable<DOMMatrix>, axis: 'x' | 'y') {
    return derived([transform],
        ([t]) => new DOMMatrix()
            .translateSelf(axis === 'x' ? t.e : 0, axis === 'y' ? t.f : 0)
            .scaleSelf(t.a, t.d, 1, 0, 0, 0)
    )
}
