import { describe, test } from 'mocha'
import { expect } from 'chai'

import { positionsAfterSorting } from './data'

describe('data', () => {
    describe('positionsAfterSorting', () => {
        test('should return the position for each element after sorting', () => {
            const values = ['c', 'a', 'b', 'd']
            const expected = [2, 0, 1, 3]
            const positions = positionsAfterSorting(values)
            expect(positions).to.eql([2, 0, 1, 3])

            const sorted = []
            positions.forEach((position, i) => {
                sorted[position] = values[i]
            })
            expect(sorted).to.eql(['a', 'b', 'c', 'd'])
        })
    })

});


