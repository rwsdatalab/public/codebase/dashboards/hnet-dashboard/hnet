import { schemeCategory10 } from 'd3-scale-chromatic'
import { derived, Readable } from 'svelte/store'
import { data, threshold } from './data'
import { focus } from './focus'

let colorScheme = schemeCategory10

export function colorWrap(i: number) {
    return colorScheme[i % colorScheme.length]
}

/**
 * Return a function that maps (i) to the color of the label (node), and (i,j) to the color of the element (arrow)
 */
export let color: Readable<(i: number, j?: number) => string> = derived(data,
    ($data) => {
        if ($data === undefined) {
            return () => 'black'
        }

        const labelColors = $data.clusters.map(colorWrap)

        // TODO reduce memory usage by not storing (all?) colors in a matrix, mostly black
        const elementColors = $data.matrix.map((el, i, j) => {
            const c1 = $data.clusters[i]
            const c2 = $data.clusters[j]
            // TODO color gradient when different??
            return c1 === c2 ? colorWrap(c1) : 'black'
        })

        return (i, j) => j === undefined ? labelColors[i] : elementColors.element(i, j)
    }
)

function normalizeOpacity(value: number, maxValue: number) {
    return value / maxValue * 0.95 + 0.05
}
/**
 * Return a function that maps (i) to the opacity of the label (node), and (i,j) to the opacity of the element (arrow)
 */
export let opacity: Readable<(i: number, j?: number) => number> = derived([data, focus, threshold],
    ([$data, $focus, $threshold]) => {
        if ($data === undefined) {
            return () => 1
        }

        if ($focus === undefined) {
            // TODO reduce memory usage by not storing (all?) opacities in a matrix, mostly 0
            const elementOpacities = $data.matrix.map((el, i, j) => {
                return normalizeOpacity(el, $data.maxValue)
            })
            return (i, j) => j === undefined ? 1 : elementOpacities.element(i, j)
        } else if ('element' in $focus) {
            const [si, sj] = $focus.element

            const labelOpacities = $data.labels.map((label, i) => i === si || i === sj ? 1 : 0.1)
            const elementOpacities = $data.matrix.map((el, i, j) => {
                const opacity = normalizeOpacity(el, $data.maxValue)
                if (i === si && j === sj) {
                    return 1
                } else {
                    return opacity * 0.1
                }
            })
            return (i, j) => j === undefined ? labelOpacities[i] : elementOpacities.element(i, j)

        } else if ('label' in $focus) {
            const si = $focus.label

            const labelOpacities = $data.matrix.row(si)
                .map((el, i) => i === si || el > $threshold ? 1 : 0.1)
            const elementOpacities = $data.matrix.map((el, i, j) => {
                const opacity = normalizeOpacity(el, $data.maxValue)
                if (i === si || j === si) {
                    return opacity
                } else {
                    return opacity * 0.1
                }
            })
            return (i, j) => j === undefined ? labelOpacities[i] : elementOpacities.element(i, j)
        }

    }
)

