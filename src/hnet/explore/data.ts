import { derived, Readable, writable } from 'svelte/store'

import type { Matrix } from '../../lib/matrix'

import { Associations, associations } from '../common/associations'
import { summarize } from './settings'
import { argSort } from '../../lib/collections'

export interface Data {
    matrix: Matrix<number>
    labels: string[]
    clusters: number[]
    proportions: number[]
    maxValue: number
    positionsSortedByLabel: number[]
    positionsSortedByCluster: number[]
}

export let allData = derived<[Readable<Associations>, Readable<boolean>], Data>([associations, summarize],
    ([$assoc, $summarize]) => {
        if (!$assoc) {
            return undefined
        }

        const matrix = $summarize ? $assoc.catMatrix : $assoc.matrix
        const labels = $summarize ? $assoc.catLabels : $assoc.labels
        const proportions = $summarize ? labels.map(() => 0) : $assoc.proportions
        const clusters = $summarize ? labels.map(() => 0) : $assoc.clusters

        return {
            matrix,
            labels,
            clusters,
            proportions,
            maxValue: matrix.isEmpty() ? 0 : matrix.reduceAll(Math.max),
            positionsSortedByLabel: positionsAfterSorting(labels),
            positionsSortedByCluster: positionsAfterSorting(clusters)
        }
    })

export const threshold = writable<number>(0)

export let data = derived<[Readable<Data>, Readable<number>], Data>(
    [allData, threshold], ([$data, $threshold]) => {
        if ($data === undefined) {
            return $data
        }

        const aboveThreshold: boolean[] = $data.matrix.toArrays()
            .map((row) => row.some((el) => el > $threshold))

        const filterThreshold = (_, index) => aboveThreshold[index]

        const matrix = $data.matrix
            .select(aboveThreshold, aboveThreshold)
            .map((el) => el > $threshold ? el : 0)
        const labels = $data.labels.filter(filterThreshold)
        const clusters = $data.clusters.filter(filterThreshold)

        return {
            matrix,
            labels,
            clusters,
            proportions: $data.proportions.filter(filterThreshold),
            maxValue: matrix.isEmpty() ? 0 : matrix.reduceAll(Math.max),
            positionsSortedByLabel: positionsAfterSorting(labels),
            positionsSortedByCluster: positionsAfterSorting(clusters)
        }
    }
)

/**
 * Returns the position for each element after sorting
 */
export function positionsAfterSorting(array: (string | number)[]): number[] {
    return argSort(argSort(array))
}