import { writable } from 'svelte/store';

export type Settings = 'labels' | 'clusters'

export const order = writable<Settings>('clusters')
export const summarize = writable<boolean>(false)