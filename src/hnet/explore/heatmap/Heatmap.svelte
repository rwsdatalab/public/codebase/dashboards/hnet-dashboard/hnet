<script lang="ts">
    import { afterUpdate, onDestroy } from 'svelte'
    import { tweened } from 'svelte/motion'
    import { cubicInOut } from 'svelte/easing'

    import { debug } from '../../common/debug'
    import { color, opacity } from '../color'
    import { data } from '../data'
    import { threshold } from '../data'
    import { order } from '../settings'
    import { toggleFocus } from '../focus'
    import { axis, zoomStore } from '../zoom'
    import type { BoundingBox } from '../zoom'
    import { svgCoordinate, svgdraggable } from '../graph/drag'

    let labelWidth = 150

    let cellSize = 20
    $: size = cellSize * $data.labels.length

    $: positionsAnimated = tweened<number[]>($data.positionsSortedByCluster, { duration: 1000, easing: cubicInOut })

    $: switch ($order) {
        case 'labels':
            positionsAnimated.set($data.positionsSortedByLabel)
            break
        case 'clusters':
            positionsAnimated.set($data.positionsSortedByCluster)
            break
    }

    $: cells = $data.matrix.toRecords()
        .filter(({ el }) => el > $threshold)

    let svg: SVGSVGElement

    let padding = 4
    let figureWidth = 1000

    let hasFocus: false
    const transform = zoomStore()
    const transformX = axis(transform, 'x')
    const transformY = axis(transform, 'y')

    $: fontSize = Math.min(1 / $transform.a, 1) + 'rem'

    let labelsGroup: SVGGElement

    let bbox: BoundingBox

    $: {
        transform.setClientWidth(figureWidth - 2 * padding)
    }

    // Do not let the label width grow beyond a max while zooming
    $: maxLabelWidth = figureWidth / 4
    $: labelWidthCropped = Math.min(labelWidth, maxLabelWidth / $transform.a)

    afterUpdate(() => {
        const labelsBox = labelsGroup?.getBBox({ stroke: true })
        if (labelsBox) {
            labelWidth = labelsBox.width
            bbox = {
                x: -labelWidth,
                y: -labelWidth,
                width: labelWidth + size,
                height: labelWidth + size
            }
            transform.setBoundingBox(bbox)
        }
    })


    const unsubscribe = data.subscribe(() => {
        transform.zoomFit()
    })

    onDestroy(unsubscribe)

    /**
     * If the graph has focus, mouse wheel will zoom in/out and prevent page scroll
     */
    function graphWheel(e: WheelEvent) {
        if (!hasFocus) {
            return
        }

        e.preventDefault()
        const origin = svgCoordinate(e, svg)
        const factor = 1 - e.deltaY / 100
        transform.zoom(factor, origin)
    }


</script>

<figure bind:clientWidth={figureWidth} tabindex="0" on:focus={() => { hasFocus = true }} on:blur={() => { hasFocus = false }}
        class="focus:outline-black border-2 border-donkerblauw-1 text-2xl" style="padding: {padding}px"
>
    <svg viewBox="0 0 {figureWidth - 2 * padding} {figureWidth - 2 * padding}" bind:this={svg} on:wheel={graphWheel}>

        <!-- grid -->
        <g transform={$transform} use:svgdraggable on:svgdragstart={transform.panStart}
           on:svgdragmove={transform.panMove}
           on:svgdragend={transform.panEnd}>
            <!-- Background -->
            <rect x="1" y="1" width={size - 2} height={size - 2} class="fill-grijs-1"/>

            <!-- lines -->
            {#each $data.labels as label, i}
                <!-- Column label and lines -->
                <line x1={$positionsAnimated[i] * cellSize} y1="0"
                      x2={$positionsAnimated[i] * cellSize} y2={size}
                      class="stroke-2 stroke-white pointer-events-none"/>
                <line x1={($positionsAnimated[i] + 1) * cellSize} y1="0"
                      x2={($positionsAnimated[i] + 1) * cellSize} y2={size}
                      class="stroke-2 stroke-white pointer-events-none"/>

                <!-- Row label and lines -->
                <line x1="0" y1={$positionsAnimated[i] * cellSize}
                      x2={size} y2={$positionsAnimated[i] * cellSize}
                      class="stroke-2 stroke-white pointer-events-none"/>
                <line x1="0" y1={($positionsAnimated[i] + 1) * cellSize}
                      x2={size} y2={($positionsAnimated[i] + 1) * cellSize}
                      class="stroke-2 stroke-white pointer-events-none"/>
            {/each}

            <!-- filtered cells -->
            {#each cells as { el, i, j }}
                <rect x={$positionsAnimated[j] * cellSize + 1} y={$positionsAnimated[i] * cellSize + 1}
                      width={cellSize - 2} height={cellSize - 2}
                      style="fill: {$color(i, j)}; fill-opacity: {$opacity(i, j)}"
                      on:dblclick={() => toggleFocus({element: [i, j]})}
                ></rect>


                {#if $debug}
                    <text x={($positionsAnimated[j] + 0.5) * cellSize + 1}
                          y={($positionsAnimated[i] + 0.5) * cellSize + 1}
                          opacity="{$opacity(i, j)}"
                          class="pointer-events-none"
                          text-anchor="middle" dominant-baseline="middle"
                          font-size={fontSize}
                    >
                        {el.toFixed(1)}
                    </text>
                {/if}
            {/each}
        </g>

        <!-- x labels -->
        <g transform={$transformX} font-size={fontSize}>
            <rect fill="white" x="0" y="0" width={size} height={labelWidthCropped}/>
            {#each $data.labels as label, i}
                <g transform="translate({($positionsAnimated[i] + 0.5) * cellSize}, {labelWidthCropped}) rotate(-90)">
                    <text text-anchor="start" dominant-baseline="middle" on:dblclick={() => toggleFocus({label: i})}>
                        {label}
                    </text>
                </g>
            {/each}
        </g>

        <!-- y labels -->
        <g transform={$transformY} font-size={fontSize}>
            <rect fill="white" x="0" y="0" width={labelWidthCropped} height={size}/>
            <g bind:this={labelsGroup}>
                {#each $data.labels as label, i}
                    <text x={labelWidthCropped} y={($positionsAnimated[i] + 0.5) * cellSize} text-anchor="end"
                          dominant-baseline="middle"
                          on:dblclick={() => toggleFocus({label: i})}
                    >{label}</text>
                {/each}
            </g>

        </g>

        <!-- top left corner -->
        <rect fill="white" x="0" y="0"
              width={labelWidthCropped * $transform.a}
              height={labelWidthCropped * $transform.a}/>

        {#if $debug}
            <rect transform={$transform} {...bbox} fill="none" class="stroke-red-500 pointer-events-none"/>
        {/if}

    </svg>
</figure>

<style>
</style>