export interface XY {
    x: number
    y: number
}

export function svgdraggable(element: SVGElement) {
    /**
     * Transformation matrix to map svg and client coordinates:
     * (clientX, clientY) = (ax + e, dy + f)
     */
    let svg: SVGSVGElement | undefined

    element.addEventListener('mousedown', handleMousedown)

    // Prevent click event propagation after drag
    element.addEventListener('click', (e) => e.stopPropagation())

    function handleMousedown(event: MouseEvent) {
        let parentElement = element.parentElement
        while (parentElement && !(parentElement instanceof SVGSVGElement)) {
            parentElement = parentElement.parentElement
        }

        if (parentElement instanceof SVGSVGElement) {
            svg = parentElement
        } else {
            throw new Error('draggable: Could not find container SVG element')
        }

        element.dispatchEvent(new CustomEvent('svgdragstart', {
            detail: svgCoordinate(event, svg)
        }));

        window.addEventListener('mousemove', handleMousemove)
        window.addEventListener('mouseup', handleMouseup)
    }

    function handleMousemove(event: MouseEvent) {
        element.dispatchEvent(new CustomEvent('svgdragmove', {
            detail: svgCoordinate(event, svg)
        }));
    }

    function handleMouseup(event: MouseEvent) {
        element.dispatchEvent(new CustomEvent('svgdragend', {
            detail: svgCoordinate(event, svg)
        }));

        window.removeEventListener('mousemove', handleMousemove)
        window.removeEventListener('mouseup', handleMouseup)
        svg = undefined
    }

    return {
        destroy() {
            // the node has been removed from the DOM
            element.removeEventListener('mousedown', handleMousedown)
        }
    }
}

export function svgCoordinate({ clientX, clientY }: MouseEvent, svg: SVGSVGElement): XY {
    if (svg && svg.getScreenCTM !== undefined) {
        const ctm = svg.getScreenCTM()
        return {
            x: (clientX - ctm.e) / ctm.a,
            y: (clientY - ctm.f) / ctm.d
        }
    } else {
        console.error('Could not determine Screen CTM from ', svg)
        return { x: clientX, y: clientY }
    }
}
