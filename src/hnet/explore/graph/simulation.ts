import { derived, Readable, writable, Writable } from 'svelte/store'
import { data } from '../data'
import {
    forceLink,
    forceManyBody,
    forceSimulation,
    forceX,
    forceY,
    Simulation,
    SimulationLinkDatum,
    SimulationNodeDatum
} from 'd3-force'

export interface Config {
    chargeStrength: number
    chargeDistanceMax: number

    minDistance: number
    maxDistance: number

    centerStrength: number
}

export const config: Writable<Config> = writable({
    chargeStrength: -1500,
    chargeDistanceMax: 300,

    minDistance: 50,
    maxDistance: 200,

    centerStrength: 0.01
})

export interface SimulationNode extends SimulationNodeDatum {
    label: string
    proportion: number
}

export interface SimulationLink extends SimulationLinkDatum<SimulationNode> {
    el: number
    i: number
    j: number
}

const initialValue = { nodes: [], links: [] }
let currentSimulation: Simulation<SimulationNode, SimulationLink>

export const simulation: Readable<{ nodes: SimulationNode[], links: SimulationLink[] }> = derived(
    [data, config],
    ([$data, $config], set) => {
        if (currentSimulation) {
            currentSimulation.stop()
            currentSimulation = undefined
        }

        const nodes: SimulationNode[] = $data.labels
            .map((label, i) => ({
                label,
                proportion: $data.proportions[i]
            }))


        const links: SimulationLink[] = $data.matrix
            .mapElements((el, i, j) => ({
                el, i, j,
                source: i,
                target: j,
            }))
            // Only one link between nodes (matrix upper triangular)
            .filter((link) => link.i < link.j)
            // Only non-zero
            .filter((link) => link.el > 0)


        currentSimulation = forceSimulation(nodes)

        currentSimulation.force('charge', forceManyBody()
            .strength($config.chargeStrength)
            .distanceMax($config.chargeDistanceMax)
        )
        currentSimulation.force('link', forceLink(links)
            .distance((link) =>
                $config.maxDistance - (link.el / $data.maxValue) * ($config.maxDistance - $config.minDistance)
            )
        )

        currentSimulation.force('x', forceX().strength($config.centerStrength))
        currentSimulation.force('y', forceY().strength($config.centerStrength))

        currentSimulation.on('tick', () => {
            set({ nodes, links })
        })
    },
    initialValue
)

export function moveNode(node: SimulationNode, { x, y }: { x: number, y: number }) {
    let reheatAlpha = 0.2
    node.fx = x
    node.fy = y
    if (currentSimulation.alpha() < reheatAlpha) {
        currentSimulation.alpha(reheatAlpha)
        currentSimulation.restart()
    }
}