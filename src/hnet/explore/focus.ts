import { writable } from 'svelte/store'
import { data } from './data'

interface FocusLabel {
    label: number
}

interface FocusElement {
    element: [number, number]
}

type Focus = FocusLabel | FocusElement | undefined

export const focus = writable<Focus>(undefined)

data.subscribe(() => {
    resetFocus()
})

export function resetFocus() {
    focus.set(undefined)
}

export function toggleFocus(s: Focus) {
    focus.update((prev) => {
        return JSON.stringify(prev) === JSON.stringify(s) ? undefined : s
    })
}
