import { Description } from './description'
import { Associations } from './associations'
import { Options } from '../data/options'

import type { Matrix } from '../../lib/matrix'

const VERSION = 2

export type OmitFunctions<T> = {
    [Key in keyof T]: T[Key] extends Function ? never : Key
}[keyof T]

/**
 * Fancy TypeScript type to replace all property types from Matrix/Set/Map to arrays and remove functions,
 * mainly usable to describe a serializable type
 */
export type Plain<Type> = {
    // Only the non-functions
    [Key in OmitFunctions<Type>]: Type[Key] extends Set<any>
        // Replace Set with array
        ? any[]
        : Type[Key] extends Map<any, any>
            // Replace Map with array
            ? [string, any][]
            : Type[Key] extends Matrix<any>
                // Replace Matrix with array-array
                ? any[][]
                : Type[Key] extends object[]
                    // Replace object array with plain-object array
                    ? Plain<Type[Key][number]>[]
                    : Type[Key] extends any[]
                        // Don't replace other arrays
                        ? Type[Key]
                        : Type[Key] extends object
                            // Replace object with plain object
                            ? Plain<Type[Key]>
                            : Type[Key]
}

export interface HnetData {
    description: Description
    options: Options
    associations: Associations
}

interface HnetPlainData extends Plain<HnetData> {
    hnetJsonVersion: typeof VERSION
}

function isCurrentVersion(obj: any): obj is HnetPlainData {
    return obj.hnetJsonVersion === VERSION
}

export function toHnetJson({ description, options, associations }: HnetData): string {
    const plainData: HnetPlainData = {
        hnetJsonVersion: VERSION,
        description: description.toPlain(),
        options: options.toPlain(),
        associations: associations.toPlain()
    }

    return JSON.stringify(plainData)
}

export function fromHnetJson(json: string): HnetData {
    const data = JSON.parse(json)

    if (isCurrentVersion(data)) {
        return {
            description: Description.fromPlain({
                ...data.description,
                fromExportFile: true
            }),
            options: Options.fromPlain(data.options),
            associations: Associations.fromPlain(data.associations)
        }
    } else {
        console.log('hnetJsonVersion', data.hnetJsonVersion)
        throw new Error('Unknown format, could not read HNet data')
    }
}

export function readHnetFile(file: File): Promise<HnetData> {
    return new Promise(((resolve, reject) => {
        try {
            const reader = new FileReader();
            reader.onerror = () => reject(reader.error)
            reader.onload = () => {
                try {
                    const data = fromHnetJson(reader.result as string)
                    resolve(data)
                } catch (e) {
                    reject(e)
                }
            }
            reader.readAsText(file);
        } catch (e) {
            reject(e)
        }
    }))

}


