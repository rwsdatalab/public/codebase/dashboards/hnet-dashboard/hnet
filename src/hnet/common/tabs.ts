import { writable } from 'svelte/store';

export enum Step {
    data = 'Data',
    explore = 'Explore'
}

export const tabs = [Step.data, Step.explore]

export const currentTab = writable<number>(0)

export function go(step: Step) {
    const i = tabs.findIndex((s) => s === step)
    if (i !== -1) {
        currentTab.set(i)
    }
}

