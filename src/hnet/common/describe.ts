import Papa from 'papaparse'

import { Description } from './description'
import type { FeatureDescription } from './description'
import { increase } from '../../lib/collections'

export function describeData(file: File): Promise<Description> {
    return new Promise((resolve, reject) => {
        const featureMap = new Map<string, FeatureDescription>()

        let rowCount = 0

        Papa.parse(file, {
            header: true,
            skipEmptyLines: true,
            step: (results) => {
                if (featureMap.size === 0) {
                    // Initialize
                    for (const field of results.meta.fields) {
                        featureMap.set(field, {
                            label: field,
                            dtype: 'cat',
                            uniqueNum: new Map<string, number>(),
                            uniqueCat: new Map<string, number>()
                        })
                    }
                }

                if (results.errors.length) {
                    results.errors.forEach((e) => {
                        console.error(e)
                    })
                    return
                }

                for (const field of results.meta.fields) {
                    // Null or undefined becomes empty string
                    const value = results.data[field] ?? ''
                    const feature = featureMap.get(field)

                    // Determine if it could be converted to a number
                    if (isNumber(value)) {
                        increase(feature.uniqueNum, value)
                    } else {
                        increase(feature.uniqueCat, value)
                    }
                }
                rowCount++
            },
            complete: () => {
                const features = Array.from(featureMap.values())
                for (const feature of features) {
                    if (bestGuessNum(feature)) {
                        feature.dtype = 'num'
                    }
                }

                resolve(new Description({
                    features,
                    shape: [rowCount, featureMap.size],
                    uploadedFile: file
                }))
            }
        })
    })
}

/** Determine if this string represents a number */
export function isNumber(value: string) {
    return !(isNaN(Number(value)) || isNaN(parseFloat(value)))
}

/** Make a best guess from the values if this features should be of dtype num */
export function bestGuessNum(feature: FeatureDescription): boolean {
    if (feature.uniqueCat.size === 0 || feature.uniqueCat.size === 1 && feature.uniqueCat.has('')) {
        // Only numbers
        let count = 0
        for (const [num, n] of feature.uniqueNum) {
            if (!Number.isInteger(Number.parseFloat(num))) {
                // Floating point, it is numeric
                return true
            }

            // Meanwhile, add up the total count for the next check
            count += n
        }

        // Integers, only numeric if there are many unique integers
        // TODO 0.8 vervangen
        return feature.uniqueNum.size / count >= 0.8;
    } else {
        // Not only numbers
        return false
    }

}