import { describe, test } from 'mocha'
import { expect } from 'chai'
import { filterValues, moreThanOneUnique } from './associate'

describe('associate', () => {
    describe('moreThanOneUnique', () => {
        test('zero', () => {
            expect(moreThanOneUnique(new Set(['NA']), [
                new Map(), new Map()
            ])).to.be.false
        })
        test('only missing', () => {
            expect(moreThanOneUnique(new Set(['NA']), [
                new Map([['NA', 2]]), new Map([['NA', 1]])
            ])).to.be.false
        })
        test('one other', () => {
            expect(moreThanOneUnique(new Set(['NA']), [
                new Map([['NA', 2], ['num1', 1]]),
                new Map([['NA', 1]])
            ])).to.be.false
        })
        test('one other in the other map', () => {
            expect(moreThanOneUnique(new Set(['NA']), [
                new Map([['NA', 2]]),
                new Map([['NA', 1], ['cat1', 3]])
            ])).to.be.false
        })
        test('two uniques', () => {
            expect(moreThanOneUnique(new Set(['NA']), [
                new Map([['NA', 2], ['num1', 1]]),
                new Map([['NA', 1], ['cat1', 3]])
            ])).to.be.true
        })
    })
    describe('filterValues', () => {
        // Chai does not display what is different when comparing Maps at the moment
        //   AssertionError: expected {} to deeply equal {}
        // https://github.com/chaijs/chai/issues/1228

        const missing = new Set(['NA'])
        const ignored = new Set(['cat0'])

        test('empty when one non-missing', () => {
            expect(filterValues(1, missing, ignored,
                new Map([['cat1', 20]])
            )).to.eql(new Map())
        })
        test('only one when one non-missing and one ignore', () => {
            expect(filterValues(1, missing, ignored,
                new Map([['cat0', 10], ['cat1', 20]])
            )).to.eql(new Map([['cat1', 20]]))
        })
        test('zero when one when one non-missing and one ignore and one not enough', () => {
            expect(filterValues(30, missing, ignored,
                new Map([['cat0', 10], ['cat1', 20]])
            )).to.eql(new Map())
        })
        test('two maps', () => {
            expect(filterValues(10, missing, ignored,
                new Map([['num0', 5], ['num1', 20]]),
                new Map([['cat0', 20], ['cat1', 50]])
            )).to.eql(new Map([['num1', 20], ['cat1', 50]]))
        })
    })
})
