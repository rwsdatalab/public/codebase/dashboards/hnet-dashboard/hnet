import { writable } from 'svelte/store'
import type { Plain } from './hnetFileFormat'

export interface FeatureDescription {
    label: string
    dtype: 'num' | 'cat'
    uniqueNum: Map<string, number>
    uniqueCat: Map<string, number>
}

export class Description {
    features: FeatureDescription[]
    shape: [number, number]
    filename: string
    uploadedFile: File
    fromExportFile = false

    constructor(partial?: Pick<Description, 'features' | 'shape' | 'uploadedFile'>) {
        if (partial) {
            this.features = partial.features
            this.shape = partial.shape
            this.uploadedFile = partial.uploadedFile
            this.filename = partial.uploadedFile.name
        }
    }

    toPlain(): Plain<Description> {
        return {
            ...this,
            features: this.features.map((f) => ({
                ...f,
                uniqueNum: Array.from(f.uniqueNum),
                uniqueCat: Array.from(f.uniqueCat)
            })),
            uploadedFile: undefined
        }
    }

    static fromPlain(desc: Plain<Description>): Description {
        // Use Partial type in between to catch type problems
        const partial: Partial<Description> = {
            ...desc,
            features: desc.features.map((f) => ({
                ...f,
                uniqueNum: new Map(f.uniqueNum),
                uniqueCat: new Map(f.uniqueCat)
            })),
            uploadedFile: undefined
        }

        return Object.assign(new Description(), partial)
    }
}

export const description = writable<Description | undefined>(undefined)