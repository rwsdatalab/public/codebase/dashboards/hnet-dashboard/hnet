import type { Description } from './description'
import { Associations } from './associations'
import type {
    HnetWorkerFeatureDescription,
    HnetWorkerMessage,
    HnetWorkerPostMessage,
    ProgressMessage,
    StepMessage
} from './hnet.worker'
import type { FeatureOption, Options } from '../data/options'

/** Handles the communication to and from the hnet.worker.js Web Worker that finds associations using HNet */
export class HnetWorker {
    private worker: Worker | undefined
    private progressHandler: (msg: ProgressMessage) => void = () => {}
    private stepHandler: (msg: StepMessage) => void = () => {}

    /** Registers a callback that will be called at each step in the worker */
    onStep(handler: (msg: StepMessage) => void) {
        this.stepHandler = handler
    }

    /** Registers a callback that will be called with the progress for this step, or -1 if this step has no progress */
    onProgress(handler: (msg: ProgressMessage) => void) {
        this.progressHandler = handler
    }

    /** Terminates the HNet Web Worker immediately */
    abort() {
        if (this.worker) {
            this.worker.terminate()
            this.worker = undefined
        }
    }

    /** Starts the HNet association learning */
    associateData(description: Description, options: Options): Promise<Associations> {
        return new Promise((resolve, reject) => {
            if (!description.uploadedFile) {
                reject(new Error('No uploaded file'))
            }

            this.worker = new Worker('build/hnet.worker.js')

            this.worker.onmessage = (e) => {
                const message = e.data as HnetWorkerMessage

                switch (message.type) {
                    case 'result':
                        console.timeEnd('hnet worker')
                        this.worker.terminate()
                        this.worker = undefined

                        resolve(Associations.fromPlain({
                            ...message,
                            date: Date.now()
                        }))
                        break
                    case 'step':
                        console.log('Step:', message.step)
                        this.stepHandler(message)
                        break
                    case 'progress':
                        this.progressHandler(message)
                        break
                    default:
                        console.error('unknown worker message', message)

                }
            }

            // Filter and transform the features and values to start HNet worker with
            const featureDescriptions = description.features
                .map((feature, i) => {
                    const featureOption = options.features[i]

                    if (!featureOption.include) return

                    const values = featureOption.dtype === 'cat'
                        ? filterCatValues(options, featureOption, feature.uniqueNum, feature.uniqueCat)
                        : filterNumValues(options, featureOption, feature.uniqueNum)

                    if (values.size === 0) return

                    return { feature: feature.label, type: featureOption.dtype, values } as HnetWorkerFeatureDescription
                })
                .filter((feature) => feature !== undefined)

            console.time('hnet worker')
            this.worker.postMessage({
                file: description.uploadedFile,
                rows: description.shape[0],
                options,
                featureDescriptions
            } as HnetWorkerPostMessage)
        })

        function filterCatValues(options: Options, featureOptions: FeatureOption, ...uniques: Map<string, number>[]): Map<string, number> {
            const nMin = Math.max(options.yMin, options.percMinNum * description.shape[0] / 100)
            return filterValues(nMin, options.missingValues, featureOptions.ignored, ...uniques)
        }

        function filterNumValues(options: Options, featureOptions: FeatureOption, uniqueNum: Map<string, number>): Map<string, number> {
            const nMin = 1
            return filterValues(nMin, options.missingValues, featureOptions.ignored, uniqueNum)
        }
    }
}

/**
 * Helper function to reduce the amount of values in the uniques maps
 *
 * @param nMin Minimum amount of times the value must occur
 * @param missing Values representing 'missing value'. NB: If less than two values remain, no values will be returned.
 * @param ignored Values that should not be considered
 * @param uniques One or more unique value maps that will be filtered. These maps cannot contain overlapping values.
 */
export function filterValues(nMin: number, missing: Set<string>, ignored: Set<string>, ...uniques: Map<string, number>[]): Map<string, number> {
    const values = new Map<string, number>()
    if (!moreThanOneUnique(missing, uniques)) {
        return values
    }

    for (const unique of uniques) {
        for (const [value, n] of unique) {
            if (missing.has(value)) continue
            if (ignored.has(value)) continue
            if (n < nMin) continue

            values.set(value, n)
        }
    }
    return values
}


export function moreThanOneUnique(missing: Set<string>, uniques: Map<string, number>[]): boolean {
    let notMissing = 0
    for (const unique of uniques) {
        for (const [value, _] of unique) {
            if (missing.has(value)) {
                continue
            }
            notMissing++
            if (notMissing > 1) {
                return true
            }
        }
    }
    return false
}



