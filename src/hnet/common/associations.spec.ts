import { describe, test } from 'mocha'
import { expect } from 'chai'
import { Associations } from './associations'
import { Matrix } from '../../lib/matrix'

describe('associations', () => {
    describe('plain', () => {
        const associations = new Associations()
        associations.labels = ['cloudy_1', 'sprinkler_1']
        associations.clusters = [0, 0]
        associations.proportions = [0.6, 0.3]
        associations.matrix = Matrix.fromArrays([[0.4, 0.6], [0.6, 0.4]])
        associations.catLabels = ['cloudy', 'sprinkler']
        associations.catMatrix = Matrix.fromArrays([[0.4, 0.6], [0.6, 0.4]])
        associations.date = Date.now()

        test('serialize and deserialize should be the same again', () => {
            const plain = associations.toPlain()
            const json = JSON.stringify(plain)
            const parsed = JSON.parse(json)
            expect(parsed).to.eql(plain)

            const restored = Associations.fromPlain(parsed)
            expect(restored).to.eql(associations)
        })

    })

})
