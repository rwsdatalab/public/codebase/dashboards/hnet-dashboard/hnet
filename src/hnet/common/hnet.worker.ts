import Papa from 'papaparse'
import { jStat } from 'jstat'
import jLouvain from 'jlouvain.js'

import { Matrix} from '../../lib/matrix'
import { combineFisher, holmCorrectP, median, rankSumTest } from '../../lib/stats'
import { argSort, increase, sum } from '../../lib/collections'
import type { Options } from '../data/options'

const ctx: Worker = self as any;

export interface ResultMessage {
    type: 'result'
    labels: string[]
    proportions: number[]
    clusters: number[]
    matrix: number[][]
    catLabels: string[]
    catMatrix: number[][]
}

export interface ProgressMessage {
    type: 'progress'
    progress: number
}

export interface StepMessage {
    type: 'step'
    step: string
}

export type HnetWorkerMessage = ResultMessage | StepMessage | ProgressMessage

export interface HnetWorkerFeatureDescription {
    type: 'num' | 'cat'
    feature: string
    values: Map<string, number>
}

export interface HnetWorkerPostMessage {
    file: File
    rows: number
    options: Options
    featureDescriptions: HnetWorkerFeatureDescription[]
}

ctx.onmessage = function hnetWorkerOnMessage(event) {
    const args = event.data as HnetWorkerPostMessage

    sendStep(`HNet analysis started`)

    // Create lookup arrays to collect data from file
    let catFeatures: string[] = []
    let category: string[] = []
    let categoryCount: number[] = []

    for (const featureDescriptions of args.featureDescriptions) {
        if (featureDescriptions.type === 'cat') {
            for (const [value, n] of featureDescriptions.values) {
                catFeatures.push(featureDescriptions.feature)
                category.push(value)
                categoryCount.push(n)
            }
        }
    }

    const catCount = catFeatures.length
    const uniqueCatFeatures = [...new Set(catFeatures)]
    const uniqueCatFeatureCount = uniqueCatFeatures.length

    sendStep(`Allocating data structures for categories`)

    /** While reading file, store number of times two categories occur together and the frequency of the category */
        // TODO is this the largest array in memory? See if we can use a typed (e.g. uint32?) array or something smart without categories in same feature, or just upper triangle
    const catCatCommon = new Array(catCount * catCount).fill(0)
    const catFrequencies = new Array(catCount).fill(0)

    let numFeatures: string[] = []

    /** While reading file, for (each num feature) x (each category), store the num values for rank-sum sample set */
    let numCatSamples: Map<number, number>[] = []

    /** While reading file, for (each num feature) x (each cat feature), store the num values for rank-sum total set */
    let numCatTotals: Map<number, number>[] = []

    sendStep(`Allocating data structures for numerical features`)

    for (const featureDescriptions of args.featureDescriptions) {
        if (featureDescriptions.type === 'num') {
            numFeatures.push(featureDescriptions.feature)

            // Create data structures to store data collected from file
            for (let i = 0; i < catCount; i++) {
                numCatSamples.push(new Map())
            }
            for (let i = 0; i < uniqueCatFeatures.length; i++) {
                numCatTotals.push(new Map())
            }
        }
    }

    const numCount = numFeatures.length

    let currentRow = 0
    let lastProgressUpdate = 0

    sendStep(`Reading and tabulating ${catCount + numCount} categories and numerical features`)
    sendProgress(0)

    Papa.parse(args.file, {
        header: true,
        skipEmptyLines: true,
        step: function hnetWorkerParseStep(row) {
            if (row.errors.length) {
                row.errors.forEach((e) => {
                    console.error(e)
                })
                return
            }

            const catSamples: boolean[] = catFeatures
                .map((feature, i) => row.data[feature] === category[i])

            // Update cat frequency and common occurrences
            for (let ci = 0; ci < catCount; ci++) {
                if (catSamples[ci]) {
                    catFrequencies[ci]++
                }

                for (let cj = 0; cj < catCount; cj++) {
                    if (ci !== cj && catSamples[ci] && catSamples[cj]) {
                        catCatCommon[ci * catCount + cj]++
                    }
                }
            }

            // Update num samples
            for (let ni = 0; ni < numCount; ni++) {
                const sample: string = row.data[numFeatures[ni]]
                const numSample: number = Number.parseFloat(sample)

                // Skip missing values
                if (Number.isNaN(numSample) || args.options.missingValues.has(sample)) {
                    continue
                }

                for (let cj = 0; cj < catCount; cj++) {
                    if (catSamples[cj]) {
                        increase(numCatSamples[ni * catCount + cj], numSample)
                    }
                }

                for (let cj = 0; cj < uniqueCatFeatureCount; cj++) {
                    if (!args.options.missingValues.has(row.data[uniqueCatFeatures[cj]])) {
                        increase(numCatTotals[ni * uniqueCatFeatureCount + cj], numSample)
                    }
                }
            }

            currentRow++
            if (Date.now() > lastProgressUpdate + 500) {
                sendProgress(currentRow / args.rows)
                lastProgressUpdate = Date.now()
            }
        },
        complete: function hnetWorkerParseComplete() {
            sendStep(`Calculating hypergeometric CDF for ${catCount} categories`)

            const pCat = Matrix.fromArray(catCatCommon, [catCount, catCount])
                .map((el, i, j) => {
                    // Set same feature to one
                    if (catFeatures[i] === catFeatures[j]) {
                        return 1
                    }

                    const p = 1 - jStat.hypgeom.cdf(
                        el - 1, // x
                        // TODO subtract missing values from rows
                        args.rows, // pop size
                        categoryCount[j], // success rate
                        categoryCount[i] // nr draws
                    )
                    return p
                })

            const catFeatureTotalFrequencies: Map<string, number> = new Map(
                uniqueCatFeatures.map((feature) => {
                    const totalFrequency = catFrequencies
                        .filter((frequency, i) => catFeatures[i] === feature)
                        .reduce(sum)

                    return [feature, totalFrequency]
                })
            )
            let catProportions = catFrequencies.map((frequency, i) => {
                const totalFrequency = catFeatureTotalFrequencies.get(catFeatures[i])
                return frequency / totalFrequency
            })

            sendStep(`Calculating Wilcoxon rank-sum for ${numCount} numerical features and ${catCount} categories`)

            const numLabelP = new Map<string, number[]>()
            const numLabelFeature = new Map<string, string>()
            const numLabelProportions = new Map<string, number[]>()

            for (let ni = 0; ni < numCount; ni++) {

                for (let cj = 0; cj < catCount; cj++) {
                    const uniqueCatFeatureIndex = uniqueCatFeatures.findIndex((f) => f === catFeatures[cj])
                    if (uniqueCatFeatureIndex === -1) {
                        throw new Error('wut feature not found')
                    }

                    // TODO only for n's > 20?
                    const samples = numCatSamples[ni * catCount + cj]
                    const total = numCatTotals[ni * uniqueCatFeatureCount + uniqueCatFeatureIndex]
                    const [z, p] = rankSumTest(samples, total)
                    const frequency = Array.from(samples.values()).reduce(sum, 0)
                    const totalFrequency = Array.from(total.values()).reduce(sum, 0)
                    const proportion = frequency / totalFrequency

                    const pAdj = pMinMax(p)
                    if (pAdj < 1) {
                        const feature = numFeatures[ni]
                        const value = median(samples)

                        const label = [
                            feature,
                            z > 0 ? 'high' : 'low',
                            Number.isInteger(value) ? value : value.toFixed(1)
                        ].join('_')

                        // Store p value and original feature
                        if (!numLabelP.has(label)) {
                            numLabelP.set(label, new Array(catCount).fill(1))
                            numLabelFeature.set(label, feature)
                        }
                        numLabelP.get(label)[cj] = p

                        // Store proportion
                        if (!numLabelProportions.has(label)) {
                            numLabelProportions.set(label, [])
                        }
                        numLabelProportions.get(label).push(proportion)
                    }
                }
            }

            const numLabels = Array.from(numLabelP.keys())
            const numProportions = numLabels.map((label) => {
                const proportions = numLabelProportions.get(label)
                return proportions.reduce(sum) / proportions.length
            })
            const pNum = Matrix.fromFunction([numLabels.length, catCount],
                (i, j) => numLabelP.get(numLabels[i])[j]
            )

            sendStep('Multiple test correction')

            // Append cat and num results
            let labels = [
                ...catFeatures.map((feature, i) => `${feature}_${category[i]}`),
                ...numLabels
            ]

            let proportions = [...catProportions, ...numProportions]
            let features = [...catFeatures, ...numLabels.map((label) => numLabelFeature.get(label))]
            let simmatP = pCat.append(pNum)

            const rows = simmatP.shape[0]
            simmatP = Matrix
                // Make square and symmetric
                .fromFunction([rows, rows], (row, col) => Math.min(
                    simmatP.has(row, col) ? simmatP.element(row, col) : 1,
                    simmatP.has(col, row) ? simmatP.element(col, row) : 1
                ))

                .map(pMinMax)

                // TODO correction on whole matrix instead of per row
                // TODO skip ones?
                // Multiple Test Correction
                .mapRow((row) => holmCorrectP(row))

            // Remove empty rows (and thus columns)
            const notEmpty = simmatP.toArrays().map((row) => row.some((p) => p < 1))
            labels = labels.filter((label, i) => notEmpty[i])
            proportions = proportions.filter((label, i) => notEmpty[i])
            features = features.filter((label, i) => notEmpty[i])
            simmatP = simmatP.select(notEmpty, notEmpty)

            sendStep('Summarizing')

            let summaryFeatures = [...new Set(features)]
            const summaryCount = summaryFeatures.length
            const summaryArrayP = new Array(summaryCount * summaryCount).fill(1)

            // TODO verify summary
            for (let i = 0; i < summaryCount; i++) {
                for (let j = 0; j < summaryCount; j++) {
                    if (i === j) continue

                    const subMatrix = simmatP.select(
                        features.map((f) => f === summaryFeatures[i]),
                        features.map((f) => f === summaryFeatures[j])
                    )
                    summaryArrayP[i * summaryCount + j] = combineFisher(subMatrix.elements)
                }
            }
            let summaryP = Matrix.fromArray(summaryArrayP, [summaryCount, summaryCount])
                .map(pMinMax)


            // -log10
            let simmatLogP = simmatP.map((el) => -Math.log10(el))
            let summaryLogP = summaryP.map((el) => -Math.log10(el))

            sendStep('Clustering')
            const edges = simmatLogP
                .mapElements((el, i, j) => ({
                    source: labels[i],
                    target: labels[j],
                    weight: el
                }))
                .filter(({ weight }) => weight > 0)

            let communityDetection = jLouvain()
                .nodes(labels)
                .edges(edges)

            let communities = communityDetection()
            let clusters = labels.map((label) => communities[label])

            sendStep('Sorting labels')

            const sortedLabelsIndex = argSort(labels)
            labels = sortedLabelsIndex.map((i) => labels[i])
            proportions = sortedLabelsIndex.map((i) => proportions[i])
            clusters = sortedLabelsIndex.map((i) => clusters[i])
            simmatP = simmatP.selectIndices(sortedLabelsIndex, sortedLabelsIndex)
            simmatLogP = simmatLogP.selectIndices(sortedLabelsIndex, sortedLabelsIndex)

            const sortedCatLabelsIndex = argSort(summaryFeatures)
            summaryFeatures = sortedCatLabelsIndex.map((i) => summaryFeatures[i])
            summaryP = summaryP.selectIndices(sortedCatLabelsIndex, sortedCatLabelsIndex)
            summaryLogP = summaryLogP.selectIndices(sortedCatLabelsIndex, sortedCatLabelsIndex)

            sendStep('Generating graphs')


            sendMessage({
                type: 'result',
                labels,
                proportions,
                clusters,
                matrix: simmatLogP.toArrays(),
                catLabels: summaryFeatures,
                catMatrix: summaryLogP.toArrays(),
            })
        }
    })
}

function sendMessage(msg: HnetWorkerMessage) {
    ctx.postMessage(msg)
}

function sendStep(step: string) {
    sendMessage({ type: 'step', step })
    sendProgress(-1)
}

function sendProgress(progress: number) {
    sendMessage({ type: 'progress', progress })
}

/**
 * If p is smaller than a minimum value, make it minimum value,
 * and if p is larger than alpha, make it one.
 */
function pMinMax(p: number): number {
    //TODO Bepalen beste MIN_VALUE; Epsilon omdat we nu niet de SF functies kunnen gebruiken maar (1 - CDF)

    // The Number.EPSILON property represents the difference between 1
    // and the smallest floating point number greater than 1.
    const MIN_VALUE = Number.EPSILON
    const ALPHA = 0.05

    if (p < MIN_VALUE) {
        return MIN_VALUE
    } else if (p > ALPHA) {
        return 1
    } else {
        return p
    }
}

