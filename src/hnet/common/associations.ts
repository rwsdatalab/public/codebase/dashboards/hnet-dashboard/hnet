import { writable } from 'svelte/store'
import { Matrix } from '../../lib/matrix'
import type { Plain } from './hnetFileFormat'

export class Associations {
    labels: string[]
    clusters: number[]
    proportions: number[]
    matrix: Matrix<number>
    catLabels: string[]
    catMatrix: Matrix<number>
    date: number

    toPlain(): Plain<Associations> {
        return {
            ...this,
            matrix: this.matrix.toArrays(),
            catMatrix: this.catMatrix.toArrays()
        }
    }

    static fromPlain(assoc: Plain<Associations>): Associations {
        // Use Partial type in between to catch type problems
        const partial: Partial<Associations> = {
            ...assoc,
            matrix: Matrix.fromArrays(assoc.matrix),
            catMatrix: Matrix.fromArrays(assoc.catMatrix),
        }

        return Object.assign(new Associations(), partial)
    }
}

export const associations = writable<Associations | undefined>(undefined)

