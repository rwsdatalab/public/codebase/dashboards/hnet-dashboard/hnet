import { describe, test } from 'mocha'
import { expect } from 'chai'
import { bestGuessNum, isNumber } from './describe'
import type { FeatureDescription } from './description'
import { increase } from '../../lib/collections'

describe('describe', () => {
    describe('isNumber true', () => {
        for (let value of ['1', '123', '012', '0.2', '.123', '1e-2', '7E9']) {
            test(`"${value}" is a number`, () => {
                expect(isNumber(value)).to.be.true
            })
        }
    })
    describe('isNumber false', () => {
        for (let value of ['', 'a', '12a', '12 12', '01.2.3', 'NaN', 'Inf', 'NA']) {
            test(`"${value}" is not a number`, () => {
                expect(isNumber(value)).to.be.false
            })
        }
    })


    describe('bestGuessNum', () => {
        test('empty is cat', () => {
            expect(bestGuessNum(withValues([]))).to.be.false
        })
        test('floats is num', () => {
            expect(bestGuessNum(withValues(['1.3', '1.2', '1.1']))).to.be.true
        })
        test('many unique ints is num', () => {
            expect(bestGuessNum(withValues(['13', '12', '11', '10']))).to.be.true
        })
        test('few unique ints is cat', () => {
            expect(bestGuessNum(withValues(['0', '1', '1', '1']))).to.be.false
        })
        test('few unique ints is cat', () => {
            expect(bestGuessNum(withValues(['0.0', '1.0', '1.0', '1.0']))).to.be.false
        })
        test('few unique floats is num', () => {
            expect(bestGuessNum(withValues(['0.0', '1.1', '1.1', '1.1']))).to.be.true
        })
        test('some non numeric makes cat', () => {
            expect(bestGuessNum(withValues(['1.3', '1.2', '1.1', 'a']))).to.be.false
        })
    })

})

/** helper function to create a FeatureDescription from an array of values */
function withValues(values: string[]): FeatureDescription {
    const uniqueNum = new Map()
    const uniqueCat = new Map()

    for (let value of values) {
        if (isNumber(value)) {
            increase(uniqueNum, value)
        } else {
            increase(uniqueCat, value)
        }
    }

    return { label: undefined, dtype: undefined, uniqueNum, uniqueCat }
}
