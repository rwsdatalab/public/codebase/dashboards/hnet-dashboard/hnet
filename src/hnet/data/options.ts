import { writable } from 'svelte/store'
import { Description, description } from '../common/description'
import type { Plain } from '../common/hnetFileFormat'

export interface FeatureOption {
    include: boolean
    dtype: 'num' | 'cat'
    ignored: Set<string>
}

export class Options {
    yMin = 5
    percMinNum = 0
    missingValues: Set<string> = new Set([''])
    features: FeatureOption[] = []

    constructor(description?: Pick<Description, 'features'>) {
        if (description) {
            this.features = description.features
                .map(({ dtype }) => ({ include: true, dtype, ignored: new Set() }))
        }
    }

    includeAllFeatures(include: boolean): Options {
        for (let feature of this.features) {
            feature.include = include
        }

        return this
    }

    toggleIgnore(featureIndex: number, value: string): Options {
        const ignored = this.features[featureIndex].ignored
        if (ignored.has(value)) {
            ignored.delete(value)
        } else {
            ignored.add(value)
        }

        return this
    }

    addMissingValue(value: string): Options {
        this.missingValues.add(value)
        return this
    }

    removeMissingValue(value: string): Options {
        this.missingValues.delete(value)
        return this
    }

    toPlain(): Plain<Options> {
        return {
            ...this,
            missingValues: Array.from(this.missingValues),
            features: this.features.map((f) => ({
                ...f,
                ignored: Array.from(f.ignored)
            }))
        }
    }

    static fromPlain(options: Plain<Options>): Options {
        // Use Partial type in between to catch type problems
        const partial: Partial<Options> = {
            ...options,
            missingValues: new Set(options.missingValues),
            features: options.features.map((f) => ({
                ...f,
                ignored: new Set(f.ignored)
            }))
        }

        return Object.assign(new Options(), partial)
    }
}

export const options = writable<Options>(new Options())

const unsubscribe = description.subscribe(($description) => {
    options.set(new Options($description))
})

// TODO onDestroy(unsubscribe)
