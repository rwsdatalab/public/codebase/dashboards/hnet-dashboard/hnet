import { describe, test } from 'mocha'
import { expect } from 'chai'
import { argSort, increase, range, sum, zip } from './collections'

describe('collections', () => {
    describe('array', () => {
        test('zip', () => {
            expect(zip(['a', 'b', 'c', 'd'], [1, 2, 3, 4], (u, v) => u + (2 * v)))
                .to.eql(['a2', 'b4', 'c6', 'd8'])
        })
    })

    describe('functions', () => {
        test('sum', () => {
            expect(sum(4, 3)).to.eql(7)
        })
    })

    describe('argSort()', () => {
        test('example', () => {
            expect(argSort([3, 1, 2])).to.eql([1, 2, 0])
        })

        test('empty', () => {
            expect(argSort([])).to.eql([])
        })

        test('stable', () => {
            expect(argSort(['a', 'a', 'a'])).to.eql([0, 1, 2])
        })
    })

    describe('range', () => {
        test('max', () => {
            expect(range(6)).to.eql([0, 1, 2, 3, 4, 5])
        })
        test('min max', () => {
            expect(range(-6, 6)).to.eql([-6, -5, -4, -3, -2, -1, 0, 1, 2, 3, 4, 5])
        })
        test('step', () => {
            const size = 600
            expect(range(-size / 2 + 100, size / 2, 100))
                .to.eql([-200, -100, 0, 100, 200])
        })
    })

    describe('increase', () => {
        test('existing', () => {
            const map = new Map([['a', 1], ['b', 2]])
            increase(map, 'b')
            expect(map).to.eql(new Map([['a', 1], ['b', 3]]))
        })
        test('new', () => {
            const map = new Map([['a', 1], ['b', 2]])
            increase(map, 'c')
            expect(map).to.eql(new Map([['a', 1], ['b', 2], ['c', 1]]))
        })
    })


})