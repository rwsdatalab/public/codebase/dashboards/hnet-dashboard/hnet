/**
 * Shape m (rows) x n (columns)
 */
type Shape = [number, number];

export class Matrix<Element> {
    readonly elements: Readonly<Element[]>
    readonly shape: Readonly<Shape>

    private constructor(elements: Element[], shape: Shape | Readonly<Shape>) {
        this.elements = elements
        Object.freeze(this.elements)
        this.shape = shape
        Object.freeze(this.shape)
    }

    /**
     * Get the element at i (row) x j (column).
     *
     * WARNING: naive implementation, return value is not specified if the indices are invalid.
     *
     * @param i
     * @param j
     */
    element(i: number, j: number): Element {
        return this.elements[i * this.shape[1] + j]
    }

    /**
     * Determines if these indices exist in the matrix.
     */
    has(i: number, j: number): boolean {
        let [m, n] = this.shape
        return 0 <= i && i < m && 0 <= j && j < n
    }

    /**
     * Creates a new matrix out of this and the other matrix, using the element-wise combination function.
     * @param other
     * @param fn - called for every element in this matrix and the other matrix
     */
    zip<Other, Result>(other: Matrix<Other>, fn: (a: Element, b: Other) => Result): Matrix<Result> {
        this.assertShape(other.shape)
        return new Matrix(
            this.elements.map((el, i) => fn(el, other.elements[i])),
            this.shape
        )
    }

    /**
     * Create a new matrix from this matrix with all the rows of the other matrix appended to it.
     */
    append(other: Matrix<Element>): Matrix<Element> {
        if (other.isEmpty()) {
            return this
        } else if (this.isEmpty()) {
            return other
        }

        const [m, n] = this.shape
        const [u, v] = other.shape

        if (n !== v) {
            throw new Error(`Can only append two matrices with the same number of columns (${n} !== ${v})`)
        }
        return new Matrix(
            [...this.elements, ...other.elements],
            [m + u, n]
        )
    }

    // -- map methods

    /**
     * Creates a new matrix using the element-wise mapping function.
     * @param fn - function that will be called with the element, and the i (row) and j (column) index
     */
    map<Result>(fn: (el: Element, i: number, j: number) => Result): Matrix<Result> {
        return new Matrix<Result>(this.mapElements<Result>(fn), this.shape)
    }

    /**
     * Creates a new matrix using a row-wise mapping function.
     * @param fn - function that will be called with a row, and the i (row) index, it should return the new row content
     */
    mapRow<Result>(fn: (row: Element[], i: number) => Result[]): Matrix<Result> {
        const newElements: Result[] = []

        const [m, n] = this.shape
        for (let i = 0; i < m; i++) {
            const row = this.elements.slice(i * n, (i + 1) * n)
            const newRow = fn(row, i)
            if (newRow.length !== n) {
                throw new Error(`mapRow function should return a new row of size ${n} but returned ${newRow.length}`)
            }

            for (let j = 0; j < n; j++) {
                newElements.push(newRow[j])
            }
        }

        return new Matrix<Result>(newElements, this.shape)
    }


    mapElements<Result>(fn: (el: Element, i: number, j: number) => Result): Result[] {
        const [m, n] = this.shape
        const newElements: Result[] = []

        for (let i = 0; i < m; i++) {
            for (let j = 0; j < n; j++) {
                newElements.push(fn(this.elements[i * n + j], i, j))
            }
        }
        return newElements
    }

    /**
     * Creates the transpose matrix, i.e. mirrored over the diagonal, i.e. rows and columns swapped.
     */
    transpose(): Matrix<Element> {
        let [m, n] = this.shape
        return Matrix.fromFunction([n, m], (i, j) => this.elements[j * n + i])
    }

    /**
     * Reduces all the elements in the array into a single number, using the function supplied.
     *
     * The reducer is first called with the first two elements and then the in between result and each new element.
     *
     * @example
     * ```
     * const maxValue: number = matrix.reduceAll(Math.max)
     * ```
     *
     * @param fn - reduce function called with the accumulator (in between result) and each element
     */
    reduceAll(fn: (acc: Element, el: Element) => Element): Element {
        // Note!! Cannot use fn directly because reduce callback is called with more arguments
        // https://stackoverflow.com/a/2856764
        return this.elements.reduce((acc, el) => fn(acc, el))
    }

    /**
     * Reduce all the rows in the matrix into a single array ("row") of numbers.
     *
     * @example
     * ```
     * Matrix.fromArrays([[1,2,3],[4,5,6],[7,8,9]]).reduceRows(Math.max) // [7,8,9]
     * ```
     *
     * @param fn - reduce function called with the accumulator (in between result) and element i of every row
     */
    reduceRows(fn: (acc: Element, el: Element) => Element): Element[] {
        const result = []
        const [m, n] = this.shape

        for (let j = 0; j < n; j++) {
            let acc = this.elements[j]
            for (let i = 1; i < m; i++) {
                const el = this.elements[i * n + j]
                acc = fn(acc, el)
            }
            result.push(acc);
        }

        return result
    }

    /**
     * Reduce all the columns in the matrix into a single array ("column") of numbers.
     *
     * @example
     * ```
     * Matrix.fromArrays([[1,2,3],[4,5,6],[7,8,9]]).reduceColumns(Math.max) // [3,6,9]
     * ```
     *
     * @param fn - reduce function called with the accumulator (in between result) and element i of every row
     */
    reduceColumns(fn: (acc: Element, el: Element) => Element): Element[] {
        let result = []
        const [m, n] = this.shape

        for (let i = 0; i < m; i++) {
            let acc = this.elements[i * n]
            for (let j = 1; j < n; j++) {
                const el = this.elements[i * n + j]
                acc = fn(acc, el)
            }
            result.push(acc);
        }

        return result
    }

    selectIndices(rows: number[], columns: number[]): Matrix<Element> {
        let newElements: Element[] = []
        let [m, n] = this.shape

        for (let i of rows) {
            for (let j of columns) {
                newElements.push(this.elements[i * n + j])
            }
        }

        return new Matrix(newElements, [rows.length, columns.length])
    }

    /**
     * Select a section of the matrix determined by two boolean arrays
     * that determine for each row and each column if they should be in the selection.
     */
    select(rows: boolean[], columns: boolean[]): Matrix<Element> {
        this.assertShape([rows.length, columns.length])

        let newElements: Element[] = []
        let [m, n] = this.shape

        for (let i = 0; i < m; i++) {
            if (rows[i]) {
                for (let j = 0; j < n; j++) {
                    if (columns[j]) {
                        newElements.push(this.elements[i * n + j])
                    }
                }
            }
        }

        const countTrue = (acc, b) => acc + (b ? 1 : 0)
        return new Matrix<Element>(newElements, [rows.reduce(countTrue, 0), columns.reduce(countTrue, 0)])
    }

    /**
     * Get one row from the matrix
     */
    row(i: number): Element[] {
        if (i < 0 || i >= this.shape[0]) {
            throw new Error(`Illegal row index ${i} for matrix with ${this.shape[0]} rows`)
        } else {
            const cols = this.shape[1]
            return this.elements.slice(i * cols, (i + 1) * cols)
        }
    }

    private assertShape([p, q]: Shape | Readonly<Shape>): void {
        const [m, n] = this.shape
        if (p !== m || q !== n) {
            throw new Error(`Shapes are not identical: (${m}, ${n}) !== (${p}, ${q})`)
        }
    }

    /**
     * Returns an array with objects ("records") containing {el, i, j}
     */
    toRecords(): { el: Element, i: number, j: number }[] {
        return this.mapElements((el, i, j) => ({ el, i, j }))
    }

    toArrays(): Element[][] {
        let result: Element[][] = []
        const [m, n] = this.shape

        for (let i = 0; i < m; i++) {
            let row = [];
            for (let j = 0; j < n; j++) {
                row.push(this.elements[i * n + j]);
            }
            result.push(row);
        }

        return result
    }

    isEmpty(): boolean {
        return this.elements.length == 0
    }

    static fromArray<T>(elements: T[], shape: Readonly<Shape>): Matrix<T> {
        if (elements.length !== shape[0] * shape[1]) {
            throw new Error(`Number of elements ${elements.length} does not correspond to shape ${shape}`)
        }
        return new Matrix(elements, shape)
    }

    static fromArrays<T>(array: T[][]): Matrix<T> {
        const m = array.length
        if (m === 0) {
            return Matrix.empty
        }

        const n = array[0].length
        for (const row of array) {
            if (row.length !== n) {
                throw new Error('Cannot construct matrix from different row sizes')
            }
        }
        if (n === 0) {
            return Matrix.empty
        }

        let flatData = [].concat(...array);

        return new Matrix(flatData, [m, n]);
    }

    static fromDiagonal(diagonal: number[]): Matrix<number> {
        const m = diagonal.length
        const elements: number[] = []

        for (let i = 0; i < m; i++) {
            for (let j = 0; j < m; j++) {
                if (i === j) {
                    elements.push(diagonal[i])
                } else {
                    elements.push(0)
                }
            }
        }
        return new Matrix(elements, [m, m])
    }

    static fromFunction<T>([m, n]: Shape, fn: (i, j) => T): Matrix<T> {
        if (m === 0 || n === 0) {
            return Matrix.empty
        }

        const elements: T[] = []
        for (let i = 0; i < m; i++) {
            for (let j = 0; j < n; j++) {
                elements.push(fn(i, j))
            }
        }
        return new Matrix<T>(elements, [m, n])
    }

    static readonly empty = new Matrix([], [0, 0])

    static zeros([m, n]: Shape): Matrix<number> {
        if (m === 0 || n === 0) {
            return Matrix.empty
        }

        return new Matrix(new Array(m * n).fill(0), [m, n])
    }

}
