import { jStat } from 'jstat'

export function holmCorrectP(pValues: readonly number[]): number[] {
    const nTests = pValues.length

    // Because we have to keep track of original index, everything is done on an array
    // of { p: pValue, i: originalIndex } objects

    // Sort and remember original index
    const sorted = pValues
        .map((p, i) => ({ p, i }))
        .sort((a, b) => a.p - b.p)

    let max = 0
    const corrected = sorted.map(({ p, i }, iSorted) => {
        if (p === 0) {
            // Still on zero
            return { p: 0, i }
        } else if (max === 1) {
            // Already reached one
            return { p: 1, i }
        } else {
            // Between 0 and 1, go calculate

            // Multiply with [nTests..1]
            const multiplied = p * (nTests - iSorted)

            // Cumulative max, clipped to 1
            max = multiplied > 1 ? 1 : Math.max(multiplied, max)

            return { p: max, i }
        }
    })

    // Sort back, remove index
    return corrected
        .sort((a, b) => a.i - b.i)
        .map(({ p, i }) => p)

}

export function rankSumTest(samples: Map<number, number>, totals: Map<number, number>): [number, number] {
    const ranks = createRanks(totals)

    let rankSum = 0
    let n1 = 0
    for (const [value, n] of samples) {
        rankSum += (ranks.get(value) * n)
        n1 += n
    }

    let total = 0
    for (const n of totals.values()) {
        total += n
    }

    const n2 = total - n1
    const expected = n1 * (n1 + n2 + 1) / 2.0
    const z = (rankSum - expected) / Math.sqrt(n1 * n2 * (n1 + n2 + 1) / 12.0)
    const prob = 2 * (1 - jStat.normal.cdf(Math.abs(z), 0, 1))

    return [z, prob]
}

export function median(numberValues: Map<number, number>): number {
    const sorted = Array.from(numberValues)
        .sort((a, b) => a[0] - b[0])

    const count = sorted.reduce((acc, [value, n]) => acc + n, 0)
    const even = Number.isInteger(count / 2)
    const middle = Math.ceil(count / 2) - 1

    let i = 0
    for (let si = 0; si < sorted.length; si++) {
        const [value, n] = sorted[si]
        if (i <= middle && middle < i + n) {
            if (even) {
                const nextValue = (middle + 1 < i + n) ? value : sorted[si + 1][0]
                return (value + nextValue) / 2
            } else {
                return value
            }
        }
        i += n
    }
}

function createRanks(numberValues: Map<number, number>): Map<number, number> {
    const sortedValues = Array.from(numberValues)
        .sort((a, b) => a[0] - b[0])

    const ranks = new Map<number, number>()

    let rank = 1
    for (const [value, n] of sortedValues) {
        // Equal (tie) values should get midpoint rank
        ranks.set(value, (2 * rank + n - 1) / 2)
        rank = rank + n
    }
    return ranks
}


export function combineFisher(pValues: readonly number[]): number {
    if (pValues.length === 0) {
        return NaN
    }

    const statistic = -2 * pValues
        .map((p) => Math.log(p))
        .reduce((a, b) => a + b, 0)

    return 1 - jStat.chisquare.cdf(statistic, 2 * pValues.length)
}



