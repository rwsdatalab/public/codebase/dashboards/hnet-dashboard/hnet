import { describe, test } from 'mocha'
import { expect } from 'chai'
import { Matrix } from './matrix'
import { sum } from './collections'

describe('matrix', () => {
    describe('Creation', () => {
        test('fromArrays', () => {
            const matrix = Matrix.fromArrays([[1, 2, 3], [4, 5, 6]])
            expect(matrix.elements).to.eql([1, 2, 3, 4, 5, 6])
            expect(matrix.shape).to.eql([2, 3])
        })
        test('fromArrays string', () => {
            const matrix = Matrix.fromArrays([['1', '2', '3'], ['4', '5', '6']])
            expect(matrix.elements).to.eql(['1', '2', '3', '4', '5', '6'])
            expect(matrix.shape).to.eql([2, 3])
        })
        test('fromDiagonal', () => {
            const matrix = Matrix.fromDiagonal([3, 4, 5])
            expect(matrix.elements).to.eql([3, 0, 0, 0, 4, 0, 0, 0, 5])
            expect(matrix.shape).to.eql([3, 3])
        })
        test('fromElements', () => {
            const matrix = Matrix.fromArray([1, 2, 3, 4, 5, 6], [2, 3])
            expect(matrix.element(1, 1)).to.eql(5)
            expect(matrix.shape).to.eql([2, 3])
        })
        test('fromFunction', () => {
            const matrix = Matrix.fromFunction([2, 3], (i, j) => `(${i},${j})`)
            expect(matrix.elements).to.eql(['(0,0)', '(0,1)', '(0,2)', '(1,0)', '(1,1)', '(1,2)'])
            expect(matrix.shape).to.eql([2, 3])
        })
        test('zeros', () => {
            const matrix = Matrix.zeros([2, 3])
            expect(matrix.elements).to.eql([0, 0, 0, 0, 0, 0])
            expect(matrix.shape).to.eql([2, 3])
        })
    })

    describe('Element, has', () => {
        test('element', () => {
            const matrix = Matrix.fromArrays([[1, 2, 3], [4, 5, 6]])
            expect(matrix.element(0, 0)).to.eql(1)
            expect(matrix.element(1, 2)).to.eql(6)
        })
        test('element illegal indices', () => {
            const matrix = Matrix.fromArrays([[1, 2, 3], [4, 5, 6]])
            expect(matrix.element(2, 1)).to.eql(undefined)
        })
        test('has', () => {
            const matrix = Matrix.fromArrays([[1, 2, 3], [4, 5, 6]])
            expect(matrix.has(0, 0)).to.be.true
            expect(matrix.has(1, 2)).to.be.true
        })
        test('has illegal indices', () => {
            const matrix = Matrix.fromArrays([[1, 2, 3], [4, 5, 6]])
            expect(matrix.has(2, 1)).to.be.false
        })
        test('has illegal indices below zero', () => {
            const matrix = Matrix.fromArrays([[1, 2, 3], [4, 5, 6]])
            expect(matrix.has(-1, 1)).to.be.false
        })
    })

    describe('Creation errors and empty', () => {
        test('Row lengths different', () => {
            expect(() => Matrix.fromArrays([[1, 2, 3], [4, 5]])).to.throw()
        })
        test('Empty array', () => {
            const m = Matrix.fromArrays([])
            expect(m.isEmpty()).to.be.true
            expect(m.elements).to.eql([])
            expect(m.shape).to.eql([0, 0])
        })
        test('Empty array array', () => {
            const m = Matrix.fromArrays([[]])
            expect(m.isEmpty()).to.be.true
            expect(m.elements).to.eql([])
            expect(m.shape).to.eql([0, 0])
        })
        test('Empty arrays array', () => {
            const m = Matrix.fromArrays([[], []])
            expect(m.isEmpty()).to.be.true
            expect(m.elements).to.eql([])
            expect(m.shape).to.eql([0, 0])
        })
        test('empty zeros', () => {
            const matrix = Matrix.zeros([0, 4])
            expect(matrix.elements).to.eql([])
            expect(matrix.shape).to.eql([0, 0])
        })
        test('Elements and shape mismatch', () => {
            expect(() => Matrix.fromArray([1, 2, 3, 4], [1, 2])).to.throw()
        })
    })

    describe('Zip', () => {
        test('zip', () => {
            const m1 = Matrix.fromArrays([[1, 2, 3], [4, 5, 6]])
            const m2 = Matrix.fromArrays([[10, 10, 10], [20, 20, 20]])
            const matrix = m1.zip(m2, (a, b) => a + b)
            expect(matrix.elements).to.eql([11, 12, 13, 24, 25, 26])
        })
        test('zip string', () => {
            const m1 = Matrix.fromArrays([['1', '2', '3'], ['4', '5', '6']])
            const m2 = Matrix.fromArrays([['10', '10', '10'], ['20', '20', '20']])
            const matrix = m1.zip(m2, (a, b) => a + b)
            expect(matrix.elements).to.eql(['110', '210', '310', '420', '520', '620'])
        })
        test('zip mixed type', () => {
            const m1 = Matrix.fromArrays([[1, 2, 3], [4, 5, 6]])
            const m2 = Matrix.fromArrays([['1', '999', '3'], ['4', '5', '6']])
            const matrix = m1.zip(m2, (a, b) => String(a) === b)
            expect(matrix.elements).to.eql([true, false, true, true, true, true])
        })
        test('zip empty', () => {
            const m1 = Matrix.fromArrays<number>([])
            const m2 = Matrix.fromArrays<number>([])
            const matrix = m1.zip(m2, (a, b) => a + b)
            expect(matrix.elements).to.eql([])
        })
    })

    describe('Append', () => {
        test('append', () => {
            const m1 = Matrix.fromArrays([[1, 2, 3], [4, 5, 6]])
            const m2 = Matrix.fromArrays([[10, 10, 10], [20, 20, 20]])
            const matrix = m1.append(m2)
            expect(matrix.elements).to.eql([1, 2, 3, 4, 5, 6, 10, 10, 10, 20, 20, 20])
            expect(matrix.shape).to.eql([m1.shape[0] + m2.shape[0], m1.shape[1]])
        })

        test('append wrong shape', () => {
            const m1 = Matrix.fromArrays([[1, 2, 3], [4, 5, 6]])
            const m2 = Matrix.fromArrays([[10, 10], [20, 20]])
            expect(() => m1.append(m2)).to.throw()
        })

        test('append with empty', () => {
            const m1 = Matrix.fromArrays([[1, 2, 3], [4, 5, 6]])
            const m2 = Matrix.empty

            const matrix = m1.append(m2)
            expect(matrix.elements).to.eql([1, 2, 3, 4, 5, 6])
            expect(matrix.shape).to.eql(m1.shape)
        })

    })

    describe('Select, row', () => {
        test('select with number array', () => {
            const matrix = Matrix.fromArrays([[1, 2, 3], [4, 5, 6], [7, 8, 9]])
            const selected = matrix.selectIndices([0, 1], [0, 2])
            expect(selected.elements).to.eql([
                1, 3,
                4, 6
            ])

        })

        test('select with boolean array', () => {
            const matrix = Matrix.fromArrays([[1, 2, 3], [4, 5, 6], [7, 8, 9]])
            const selected = matrix.select([true, true, false], [true, false, true])
            expect(selected.elements).to.eql([
                1, 3,
                4, 6
            ])

        })

        test('row', () => {
            const matrix = Matrix.fromArrays([[1, 2, 3], [4, 5, 6], [7, 8, 9]])
            expect(matrix.row(0)).to.eql([1, 2, 3])
            expect(matrix.row(1)).to.eql([4, 5, 6])
            expect(matrix.row(2)).to.eql([7, 8, 9])
        })

        test('row below zero', () => {
            const matrix = Matrix.fromArrays([[1, 2, 3], [4, 5, 6], [7, 8, 9]])
            expect(() => matrix.row(-1)).to.throw()
        })

        test('row outside shape', () => {
            const matrix = Matrix.fromArrays([[1, 2, 3], [4, 5, 6], [7, 8, 9]])
            expect(() => matrix.row(3)).to.throw()
        })
    })

    describe('Map, transpose', () => {
        test('map', () => {
            const matrix = Matrix.fromArrays([[1, 2, 3], [4, 5, 6], [7, 8, 9]])
                .map((el, i, j) => el * 100 + i * 10 + j)
            expect(matrix.elements).to.eql([
                100, 201, 302,
                410, 511, 612,
                720, 821, 922
            ])
        })
        test('mapRow', () => {
            const matrix = Matrix.fromArrays([[1, 2, 3], [4, 5, 6], [7, 8, 9]])
                .mapRow((row, i) => row.map((el) => el * i))
            expect(matrix.elements).to.eql([0, 0, 0, 4, 5, 6, 14, 16, 18])
        })
        test('map empty', () => {
            const matrix = Matrix.fromArrays([]).map(() => 42)
            expect(matrix.elements).to.eql([])
        })
        test('transpose', () => {
            const t = Matrix.fromArrays([[1, 2, 3], [4, 5, 6], [7, 8, 9]]).transpose()
            expect(t.elements).to.eql([1, 4, 7, 2, 5, 8, 3, 6, 9])
            expect(t.shape).to.eql([3, 3])
        })
        test('transpose non-square', () => {
            const t = Matrix.fromArrays([[1, 2, 3], [4, 5, 6]]).transpose()
            expect(t.elements).to.eql([1, 4, 2, 5, 3, 6])
            expect(t.shape).to.eql([3, 2])
        })
        test('transpose empty', () => {
            const t = Matrix.fromArrays([]).transpose()
            expect(t.elements).to.eql([])
            expect(t.shape).to.eql([0, 0])
        })
    })

    describe('Reduce', () => {
        const matrix = Matrix.fromArrays([[1, 2, 3], [4, 5, 6], [7, 8, 9]])
        const empty = Matrix.fromArrays([])

        test('Reduce all', () => {
            expect(matrix.reduceAll(sum)).to.eql(45)
        })
        test('Reduce all empty', () => {
            expect(() => empty.reduceAll(sum)).to.throw()

        })
        test('Reduce rows', () => {
            expect(matrix.reduceRows(sum)).to.eql([12, 15, 18])
        })
        test('Reduce rows empty', () => {
            expect(empty.reduceRows(sum)).to.eql([])
        })
        test('Reduce columns', () => {
            expect(matrix.reduceColumns(sum)).to.eql([6, 15, 24])
        })
        test('Reduce columns empty', () => {
            expect(empty.reduceColumns(sum)).to.eql([])
        })
    })

    describe('Reduce string', () => {
        const matrix = Matrix.fromArrays([['1', '2', '3'], ['4', '5', '6'], ['7', '8', '9']])

        test('Reduce all', () => {
            expect(matrix.reduceAll((a, b) => a + b)).to.eql('123456789')
        })
        test('Reduce columns', () => {
            expect(matrix.reduceColumns((a, b) => a + b)).to.eql(['123', '456', '789'])
        })
    })

    // describe('compiler errors -- UNCOMMENT TO SEE TYPESCRIPT ERRORS', () => {
    //     test('elements can not be mutated', () => {
    //         const matrix = Matrix.fromArrays([[1, 2, 3], [4, 5, 6]])
    //         matrix.elements[0] = 3
    //     })
    //     test('elements property can not be changed', () => {
    //         const matrix = Matrix.fromArrays([[1, 2, 3], [4, 5, 6]])
    //         matrix.elements = [1, 2, 3]
    //     })
    // })
});


