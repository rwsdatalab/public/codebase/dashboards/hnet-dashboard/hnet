/**
 * Helper function to add two numbers, e.g. for matrix reduce functions.
 */
export function sum(a: number, b: number): number {
    return a + b;
}

export function zip<U, V, W>(us: U[], vs: V[], fn: (U, V) => W): W[] {
    if (us.length !== vs.length) {
        throw new Error('Could not zip unequal length arrays')
    }

    return us.map((u, i) => fn(u, vs[i]))
}

/**
 * Returns the indices that would sort an array.
 */
export function argSort(array: readonly (string | number)[]): number[] {
    const indices = Array.from(Array(array.length).keys())
    return indices.sort((a, b) => {
        if (array[a] < array[b]) {
            return -1
        } else if (array[a] > array[b]) {
            return 1
        } else {
            return 0
        }
    })
}

export function range(max: number): number[]
export function range(min: number, max: number): number[]
export function range(min: number, max: number, step: number): number[]
export function range(min: number, max?: number, step?: number): number[] {
    if (arguments.length === 1) {
        return range(0, min, 1)
    } else if (arguments.length === 2) {
        return range(min, max, 1)
    } else if (arguments.length === 3) {
        const result = []
        for (let i = min; i < max; i += step) {
            result.push(i)
        }
        return result
    } else {
        throw new Error('Range: incorrect nr of arguments')
    }
}

export function increase<K>(map: Map<K, number>, key: K) {
    const n = map.has(key) ? map.get(key) : 0
    map.set(key, n + 1)
}