# HNet Dashboard

This is the web implementation of the [Python HNet library](https://github.com/erdogant/hnet) by Erdogan Taskesen.

## Technology stack
The following technologies are used for this web application:
* Programming Language : [TypeScript](https://www.typescriptlang.org/)
* Frontend framework : [Svelte](https://svelte.dev/)
* CSS framework : [Tailwind CSS](https://tailwindcss.com/)
* Build tooling : [NodeJS](https://nodejs.org/)
* Build and hosting platform : [Gitlab CI/CD](https://docs.gitlab.com/ee/ci/quick_start/index.html) and [Gitlab Pages](https://docs.gitlab.com/ee/user/project/pages/)

The versions and other libraries in use can be found in the [package.json](./package.json).

More technical details can be found in the [ARCHITECTURE.md](./ARCHITECTURE.md) file.

## Local development

* Install latest LTS version of [NodeJS](https://nodejs.org/)
* [Clone](https://docs.gitlab.com/ee/gitlab-basics/start-using-git.html#clone-a-repository) the hnet-dashboard repository
* Navigate on the command line to the project folder
* Install libraries
```bash
npm install
```

* Start the web app on localhost

```bash
npm run dev
```

The sources are now watched and compiled to the `public/build/` folder, and the result can be seen at [http://localhost:5000/](http://localhost:5000/).

## Automated tests

The test specifications can be found alongside the sources, and are named `*.spec.ts`. For instance `matrix.spec.ts` is the test suite for the `matrix.ts` source file.

All tests can be run with the following command
```bash
npm run test
```

## Building for production

The following command can be used to create an optimized build.
```bash
npm run build
```

The sources will be compiled into the `/public/build` folder, and the `/public` folder can now be deployed and served by any web server.

The [.gitlab-ci.yml](./.gitlab-ci.yml) file contains instruction for Gitlab to build every commit on the master branch and deploy it to Gitlab pages.



