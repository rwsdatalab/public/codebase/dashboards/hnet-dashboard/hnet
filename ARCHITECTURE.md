# HNet Dashboard Architecture

## Introduction

This file contains more technical details on the architectural choices and implementations.

## Folder structure

### public

Contains static files such as the index.html that loads the application, and images. When compiling the application, the
public/build folder will contain the compiled bundles.

### src

The App.svelte and main.ts are the entry points for the application that will be loaded into the index.html

### src/ui

UI components with the look-and-feel of
the [Rijkshuisstijl](https://www.rijkshuisstijl.nl/basiselementen/basiselementen-online)

### src/lib

Data structures and libraries that are not HNet specific, such as a matrix structure and statistical methods.

### src/hnet

HNet dashboard files, structured as followed.

#### src/hnet/data

All Svelte components and stores related to the Data step in the application.

#### src/hnet/explore

All Svelte components and stores related to the Explore step in the application.

#### src/hnet/common

All Svelte stores that are common to the data and explore parts of the application.

## Styling

There are several ways of using CSS and for this project
the [atomic or utility-first](https://css-tricks.com/growing-popularity-atomic-css/) methodology is used,
using [Tailwind CSS](https://tailwindcss.com/).

In [tailwind.config.js](./tailwind.config.js) the default theme is extended
with [Rijkshuisstijl colors](https://www.rijkshuisstijl.nl/basiselementen/basiselementen-online/online-kleuren), and
some extra Tailwind options.

In [src/GlobalCss.svelte](./src/GlobalCss.svelte) some utility classes are added for this project.

## Network graph

To iteratively calculate the association network graph, a simulation of physical forces is done
using [d3-force](https://github.com/d3/d3-force) in [simulation.ts](./src/hnet/explore/graph/simulation.ts) and then
rendered on the screen using the Svelte component [Graph.svelte](./src/hnet/explore/graph/Graph.svelte).

## Web Worker

Because JavaScript in the browser is single threaded, long computations will block user interactions. One solution for
this problem is by doing the long calculation inside of
a [Web Worker](https://developer.mozilla.org/en-US/docs/Web/API/Web_Workers_API).

In this project the reading of the CSV file and calculating the associations is done in
the [hnet.worker.ts](./src/hnet/common/hnet.worker.ts) file. Because Web Workers require a separate JavaScript file to
be loaded by the browser, this file and its dependencies are compiled and bundled in a separate bundle. This is
configured in the [rollup.config.js](./rollup.config.js).

## Statistical functions

The JavaScript ecosystem for numerical and statistical calculations is not as rich as that of Python. In this project
the following libraries are used.

* For community (cluster) detection : [jLouvain.js](https://github.com/upphiminn/jLouvain)
* For probability distributions : [jStat](https://github.com/jstat/jstat)

Other methods are implemented in the project itself, using [SciPy](https://www.scipy.org/) as the reference.

