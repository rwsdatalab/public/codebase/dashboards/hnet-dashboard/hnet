const production = !process.env.ROLLUP_WATCH; // or some other env var like NODE_ENV
module.exports = {
    future: { // for tailwind 2.0 compat
        purgeLayersByDefault: true,
        removeDeprecatedGapUtilities: true,
    },
    plugins: [],
    purge: {
        content: [
            "./src/**/*.svelte",
            // may also want to include base index.html
        ],
        // enabled: true,
        enabled: false,
        // enabled: production // disable purge in dev
    },
    variants: {
        fill: ['hover'],
    },
    theme: {
        extend: {
            colors: {
                'donkerblauw': '#01689b',
                'donkerblauw-hover': '#01496d',
                'donkerblauw-1': '#cce0f1',
                'hemelblauw': '#007bc7',
                'geel': '#f9e11e',
                'geel-1': '#fdf6bb',
                'grijs-1': '#f3f3f3',
                'grijs-2': '#e6e6e6',
                'grijs-3': '#cccccc',
                'grijs-4': '#b4b4b4',
                'grijs-5': '#999999',
                'grijs-6': '#696969',
                'grijs-7': '#535353',
            },
            fill: theme => ({
                'donkerblauw': theme('colors.donkerblauw'),
                'donkerblauw-1': theme('colors.donkerblauw-1'),
                'hemelblauw': theme('colors.hemelblauw'),
                'grijs-1': theme('colors.grijs-1'),
                'grijs-2': theme('colors.grijs-2'),
                'grijs-3': theme('colors.grijs-3'),
                'grijs-4': theme('colors.grijs-4'),
                'white': theme('colors.white'),
            }),
            stroke: theme => ({
                'white': theme('colors.white'),
                'red': theme('colors.red'),
                'donkerblauw': theme('colors.donkerblauw'),
                'grijs-4': theme('colors.grijs-4'),
                'grijs-5': theme('colors.grijs-5'),
                'grijs-6': theme('colors.grijs-6'),
            }),
            strokeWidth: {
                '3': '3',
                '4': '4',
                '6': '6',
                '8': '8',
            }
        }
    },
};